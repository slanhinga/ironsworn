Character Name
==============

* **Culture/Home**
* **Title/Role**

| **EDGE** | **HEART** | **IRON** | **SHADOW** | **WITS** |
|:--------:|:---------:|:--------:|:----------:|:--------:|
|    1     |     1     |    2     |     3      |    2     |

**Experience**

    ..... ..... ..... ..... .....
    ..... ..... ..... ..... .....

**Bonds**

| o  |    |    |    |    |    |    |    |    |     |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |

* **Ranna Erin**
  - grandmother who raised me after mother died in childbirth 
  - she is also an Earth Speaker

* **Cinderhome**
    - my home town
    - known for metalwork
    - it is said that the old families of Cinderhome have black iron in their
      blood
    - it has an industrial feel, with plumes of black smoke, and soot covering
      surfaces. 

-------------------------------------------------------------------------------
# VOWS

> *troublesome* 3X, *dangerous* 2X, *formidable* 1X, *extreme* 2., *epic* 1.

------------------------------------------------------------------------------
**I will find a teacher to become Earth Speaker** (*extreme*: 2.)

|    |    |    |    |    |    |    |    |    |     |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |

------------------------------------------------------------------------------
**Vow** (*dangerous*)

|    |    |    |    |    |    |    |    |    |     |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |

------------------------------------------------------------------------------
**Vow** (*dangerous*)

|    |    |    |    |    |    |    |    |    |     |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |

-------------------------------------------------------------------------------
# TRACKS

**Momentum**

| -6 | -5 | -4 | -3 | -2 | -1 | 0 | +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |
|:--:|:--:|:--:|:--:|:--:|:--:|---|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| -  | -  | -  | -  | -  | -  | - | -  | X  | -  | -  | -  | -  | -  | -  | -  |  -  |

| Max | Reset |
|:---:|:-----:|
| +10 |  +2   |

**Health**

| .  | .  | .  | .  | X  |
|:--:|:--:|:--:|:--:|:--:|
| +1 | +2 | +3 | +4 | +5 |

**Spirit**

| .  | .  | .  | .  | X  |
|:--:|:--:|:--:|:--:|:--:|
| +1 | +2 | +3 | +4 | +5 |

**Supply**

| .  | .  | .  | .  | X  |
|:--:|:--:|:--:|:--:|:--:|
| +1 | +2 | +3 | +4 | +5 |

------------------------------------------------------------------------------
# Debilities

**Conditions**

* [ ] wounded
* [ ] shaken
* [ ] unprepared
* [ ] encumbered

**Banes**

* [ ] maimed
* [ ] corrupted

**Burdens**

* [ ] cursed
* [ ] tormented

-------------------------------------------------------------------------------
# Gear/Notes

------------------------------------------------------------------------------
# Assets

## Herbalist (path)

* [X] When you attempt to *Heal* using herbal remedies, and you have at least +1
      supply, choose one (decide before rolling). 
        * Add +2. 
        * On a hit, take or give an additional +1 health.

* [ ] When you *Heal* a companion, ally, or other character, and score a hit, take
      +1 spirit or +1 momentum.

* [ ] When you *Make Camp* and choose the option to partake, you can create a
      restorative meal. If you do, you and your companions take +1 health. Any
      allies who choose to partake also take +1 health, and do not suffer
      -supply.

## Wayfinder (path)

* [X] When you *Undertake a Journey*, take +1 momentum on a strong hit. If you
      burn momentum to improve your result, also take +1 momentum after you
      reset.

* [ ] When you *Secure an Advantage* or *Gather Information* by carefully surveying
      the landscape or scouting ahead, add +1 and take +1 momentum on a hit.

* [ ] When you *Swear an Iron Vow* to safely guide someone on a perilous journey,
      you may reroll any dice. When you *Fulfill Your Vow* and mark experience,
      take +1 experience.

## Sighted (path)

* [X] When you Face Danger or Gather Information to identify or detect mystic
      forces, add +1 and take +1 momentum on a hit.

* [ ] When you Compel, Forge a Bond, or Test Your Bond with a fellow mystic or
      mystical being, add +1 and take +1 momentum on a hit.

* [ ] When you *Secure an Advantage* by studying someone or something in a charged
      situation, add +1 and take +1 momentum on a hit. When you also pierce the
      veil to explore deeper truths (decide before rolling), you may reroll any
      dice. If you do, count a weak hit as a miss.
