Draven Yuda
===========

# Contents

- [Biography](#Biography)
    - [Stats](#Biography#Stats)
    - [Experience](#Biography#Experience)
    - [Bonds](#Biography#Bonds)
        - [Ranna Erin](#Biography#Bonds#Ranna Erin)
        - [Cinderhome](#Biography#Bonds#Cinderhome)
- [VOWS](#VOWS)
- [STATUS TRACKS](#STATUS TRACKS)
- [Debilities](#Debilities)
- [Gear/Notes](#Gear/Notes)
- [Assets](#Assets)
    - [Herbalist (path)](#Assets#Herbalist (path))
    - [Sunderer (combat talent)](#Assets#Sunderer (combat talent))
    - [Sighted (path)](#Assets#Sighted (path))
- [STORY AND MOVE TRACKS](#STORY AND MOVE TRACKS)
    - [`Undertake A Journey`: Cinderhome to Lost Rock (*troublesome*: 3 progress per waypoint)](#STORY AND MOVE TRACKS#`Undertake A Journey`: Cinderhome to Lost Rock (*troublesome*: 3 progress per waypoint))

# Biography


* **Culture/Home:** Cinderhome 
* **Title/Role:** *Listener* candidate

## Stats

| **EDGE** | **HEART** | **IRON** | **SHADOW** | **WITS** |
|:--------:|:---------:|:--------:|:----------:|:--------:|
|    1     |     2     |    1     |     3      |    2     |

## Experience

> **x**: unused experience, **0** used experience

    ..... ..... ..... ..... .....
    ..... ..... ..... ..... .....

## Bonds

| o  |    |    |    |    |    |    |    |    |     |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |

### Ranna Erin
  - grandmother who raised me after mother died in childbirth 
  - she knows some herbalism, and simple charms
  - she works as a hedge witch in **Cinderhome** - a touch of mystery in an
    industrial urban space

### Cinderhome
    - my home town
    - known for metalwork
    - it is said that the old families of Cinderhome have black iron in their
      blood
    - it has an industrial feel, with plumes of black smoke, and soot covering
      surfaces. 


-------------------------------------------------------------------------------
# VOWS

> *troublesome:* 3X, *dangerous:* 2X, *formidable:* 1X, *extreme:* 2., *epic:* 1.

------------------------------------------------------------------------------
*I will find a teacher to become a *Listener** 
(**background vow**) [*extreme*: 2.]

|    |    |    |    |    |    |    |    |    |     |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |

------------------------------------------------------------------------------
*I will determine what trouble, if any, has afflicted **Lost Rock*** 
[*dangerous*: 2X)

|    |    |    |    |    |    |    |    |    |     |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |

------------------------------------------------------------------------------
**Vow** (*dangerous*)

|    |    |    |    |    |    |    |    |    |     |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |

-------------------------------------------------------------------------------
# STATUS TRACKS

## Momentum

| -  | -  | -  | -  | -  | -  | - | -  | -  | -  | X  | -  | -  | -  | -  | -  |  -  |
|:--:|:--:|:--:|:--:|:--:|:--:|---|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| -6 | -5 | -4 | -3 | -2 | -1 | 0 | +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |

| Max | Reset |
|:---:|:-----:|
| +10 |  +2   |

## Health

| .  | X  | .  | .  | .  |
|:--:|:--:|:--:|:--:|:--:|
| +1 | +2 | +3 | +4 | +5 |

## Spirit

| .  | X  | .  | .  | .  |
|:--:|:--:|:--:|:--:|:--:|
| +1 | +2 | +3 | +4 | +5 |

## Supply

| .  | .  | .  | X  | .  |
|:--:|:--:|:--:|:--:|:--:|
| +1 | +2 | +3 | +4 | +5 |

------------------------------------------------------------------------------
# Debilities

**Conditions**

* [ ] wounded
* [ ] shaken
* [ ] unprepared
* [ ] encumbered

**Banes**

* [ ] maimed
* [ ] corrupted

**Burdens**

* [ ] cursed
* [ ] tormented

-------------------------------------------------------------------------------
# Gear/Notes

* ***Name Blade*** - a coming of age blade. a simple, single edged blade, with a
  nominal guard, a horn grip, and an unadorned bronze pommel.

* An axe. It has a well-worn oak haft, which has only become smoother, but
  stronger with time. The lacing on the grip is clearly new, but already showing
  signs of use. The blade itself is dark, dull gray, with a spiderweb of *black
  iron* spread across the surface. 

------------------------------------------------------------------------------
# Assets

* 

## Herbalist (path)

* [X] When you attempt to *Heal* using herbal remedies, and you have at least +1
      supply, choose one (decide before rolling). 
        * Add +2. 
        * On a hit, take or give an additional +1 health.

* [ ] When you *Heal* a companion, ally, or other character, and score a hit, take
      +1 spirit or +1 momentum.

* [ ] When you *Make Camp* and choose the option to partake, you can create a
      restorative meal. If you do, you and your companions take +1 health. Any
      allies who choose to partake also take +1 health, and do not suffer
      -supply.

## Sunderer (combat talent)

If you wield an axe...

* [X] When you *Strike* or *Clash* in close quarters, you may suffer -1 momentum and
      inflict +1 harm on a hit (decide before rolling).

* [ ] When you have your axe in hand, and use the promise of violence to
      *Compel* or *Secure an Advantage*, add +1 and take +1 momentum on a hit.

* [ ] When you make a tribute to a fallen foe (formidable or greater) by carving
      a rune in the haft of your axe, roll +heart. On a strong hit, inflict +1d6
      harm (one time only) when you *Strike* or *Clash*. On a weak hit, as above,
      but this death weighs on you; *Endure Stress* (2 stress).


## Sighted (path)

* [X] When you Face Danger or Gather Information to identify or detect mystic
      forces, add +1 and take +1 momentum on a hit.

* [ ] When you Compel, Forge a Bond, or Test Your Bond with a fellow mystic or
      mystical being, add +1 and take +1 momentum on a hit.

* [ ] When you *Secure an Advantage* by studying someone or something in a charged
      situation, add +1 and take +1 momentum on a hit. When you also pierce the
      veil to explore deeper truths (decide before rolling), you may reroll any
      dice. If you do, count a weak hit as a miss.

------------------------------------------------------------------------------
# STORY AND MOVE TRACKS

------------------------------------------------------------------------------
## `Undertake A Journey`: Cinderhome to Lost Rock (*troublesome*: 3 progress per waypoint)

| X  | X  | X  |    |    |    |    |    |    |     |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |


------------------------------------------------------------------------------

> Ticks use the following progression: **. o 0 X**

    vim:tw=100
