Circles
=======

# Your Truths

These questions and answers come from the world building section.

## Communities

> We live in communities called circles. These are settlements ranging in size
from a steading with a few families to a village of several hundred. Some
circles belong to nomadic folk. Some powerful circles might include a cluster of
settlements. We trade (and sometimes feud) with other circles.
>
> *Quest Starter: A decades-long feud between two circles has flared into open
conflict. What is the cause of this dispute? Do you join in the fight, or swear
to put a stop to it?*

## Leaders

> Leadership is as varied as the people. Some communities are governed by the head
of a powerful family. Or, they have a council of elders who make decisions and
settle disputes. In others, the priests hold sway. For some, it is duels in the
circle that decide.
>
> *Quest Starter: You have vivid reoccurring dreams of an Ironlands city. It has
strong stone walls, bustling markets, and a keep on a high hill. And so many
people! Nowhere in the Ironlands does such a city exist. In your dreams, you are
the ruler of this city. Somehow, no matter how long it takes, you must make this
vision a reality.*

