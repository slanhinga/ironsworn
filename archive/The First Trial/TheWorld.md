The World
=========

# Contents

- [YOUR TRUTHS](#YOUR TRUTHS)
    - [The Old World](#YOUR TRUTHS#The Old World)
    - [Iron](#YOUR TRUTHS#Iron)
    - [Legacies](#YOUR TRUTHS#Legacies)
    - [Communities](#YOUR TRUTHS#Communities)
    - [Leaders](#YOUR TRUTHS#Leaders)
    - [Defense](#YOUR TRUTHS#Defense)
    - [Mysticism](#YOUR TRUTHS#Mysticism)
    - [Religion](#YOUR TRUTHS#Religion)
    - [Firstborn](#YOUR TRUTHS#Firstborn)
    - [Beasts](#YOUR TRUTHS#Beasts)
    - [Horrors](#YOUR TRUTHS#Horrors)
    - [Other Notes](#YOUR TRUTHS#Other Notes)

# YOUR TRUTHS

I selected all of these randomly from the default set of truths.

## The Old World

> The Old World could no longer sustain us. We were too large in number. We had
felled the forests. Our crops withered in the barren ground. The cities and
villages overflowed with desperate, hungry people. Petty kings battled for
scraps. We cast our fate to the sea and found the Ironlands. A new world. A
fresh start. 
>
> *Quest Starter: Decades ago, the exodus ended. Since then, no ships have sailed
here from the Old World. Until now. Word comes of a single ship, newly arrived
across the vast ocean, grounded on the rocks of the Barrier Islands. When you
hear the name of this ship, you swear to uncover the fate of its passengers. Why
is it so important to you?*

## Iron

> The imposing hills and mountains of the Ironlands are rich in iron ore. Most
prized of all is the star-forged black iron.
>
> *Quest Starter: The caravan, bound for the distant southlands, left the mining
settlement last season but never arrived at its destination. It carried a bounty
of black iron. Why is finding this lost caravan so important to you?*

## Legacies

> Other humans sailed here from the Old World untold years ago, but all that is
left of them is a savage, feral people we call the broken. Is their fate to
become our own?
>
> *Quest Starter: You find a child—one of the broken. It is wounded, and hunted by
others of its kind. Do you protect it, even at the risk of inviting the wrath of
the broken tribes?*

## Communities

> We live in communities called circles. These are settlements ranging in size
from a steading with a few families to a village of several hundred. Some
circles belong to nomadic folk. Some powerful circles might include a cluster of
settlements. We trade (and sometimes feud) with other circles.
>
> *Quest Starter: A decades-long feud between two circles has flared into open
conflict. What is the cause of this dispute? Do you join in the fight, or swear
to put a stop to it?*

## Leaders

> Leadership is as varied as the people. Some communities are governed by the head
of a powerful family. Or, they have a council of elders who make decisions and
settle disputes. In others, the priests hold sway. For some, it is duels in the
circle that decide.
>
> *Quest Starter: You have vivid reoccurring dreams of an Ironlands city. It has
strong stone walls, bustling markets, and a keep on a high hill. And so many
people! Nowhere in the Ironlands does such a city exist. In your dreams, you are
the ruler of this city. Somehow, no matter how long it takes, you must make this
vision a reality.*

## Defense

> Our warbands are rallied to strike at our enemies or defend our holdings. Though
not nearly as impressive as the armies that once marched across the Old World,
these forces are as well-trained and equipped as their communities can manage.
The banners of the warbands are adorned with depictions of their Old World
history and Ironland victories.
>
> *Quest Starter: A warband was wiped out in a battle against an overwhelming
enemy. What is your connection to this band? Who defeated them? Will you carry
their banner on a quest for vengeance, or do you vow to see it brought home to a
place of honor?*

## Mysticism

> Magic is rare and dangerous, but those few who wield the power are truly gifted.
>
> *Quest Starter: You have heard stories of someone who wields true power. They
live in an isolated settlement far away. Who told you of this mystic? Are they
feared or respected? Why do you swear to seek them out?*

## Religion

> The people honor old gods and new. In this harsh land, a prayer is a simple but
powerful comfort.
>
> *Quest Starter: An Ironlander is determined to make a pilgrimage into dangerous
lands. What holy place do they seek? Why do you swear to aid them on this
journey? Who seeks to stop them and why?*

## Firstborn

> The firstborn live in isolation and are fiercely protective of their own lands.
>
> *Quest Starter: The elf, outcast from his kind, lives with Ironlanders. Over
time, he became a part of the community. Now, he is dying. He yearns to return
to his people before he passes. Does he seek absolution or justice? Why do you
swear to help him? What force opposes his return?*

## Beasts

> The beasts of old are nothing but legend. A few who travel into the deep forests
and high mountains return with wild tales of monstrous creatures, but they are
obviously delusional. No such things exist.
>
> *Quest Starter: You were witness to an attack by what you thought was an animal
of monstrous proportions. No one believes you. In fact, you are accused of the
murder you blame on this beast. How can you prove your innocence? Can you even
trust your own memories of the event?*

## Horrors

> We are wary of dark forests and deep waterways, for monsters lurk in those
places. In the depths of the long-night, when all is wreathed in darkness, only
fools venture beyond their homes.
>
> *Quest Starter: You bear the scars of an attack by a horror. What was it? Are
those scars physical, emotional, or both? How do you seek to make yourself whole
again?*

## Other Notes
