Cinderhome
==========

**Cinderhome** is located in the *[Havens](Havens.md)*, near the border with the
*[Hinterlands](Hinterlands.md)*. Rivers provide access for iron ore, mined in the hinterlands, and
for trade with the rest of the **Ironlands**. As [circles](circles.md) go, it is a larger
community, including both a well developed docks district, a bustling central
city, and surrounding farmland. There is a small upper class, consisting of a
half-dozen major families who first settled the area. Despite the harsh
conditions, they enjoy a certain degree of luxury, taking advantage of the hot
springs and geothermal heating in the region.

The *circle* of **Cinderhome** abuts a dark forest. Logging not only provides
valuable resources for the city, it is also essential to keeping the wilds at
bay, so they do not encroach upon the small oasis of civilization.

>    **Oracle 8: Quick Settlement Name Generator**
    
>    * 69 - Lost-
>    * 78 - rock

> ***Quest idea:*** - For three weeks, there has been no word from the camp
**Lost Rock**, a small but permanent community, built around an iron mine.
Yesterday, a body floated into the docks district from the direction of 
**Lost Rock**, with strange wounds on the body. 

Smithing holds such a central place in **Cinderhome** society, and basically
everyone has a basic understanding of the craft. It is a mark of transition to
adulthood when a child crafts a knife on their own. This practice culminates in
a "sheathing ceremony." This comming of age rite is common to all genders, and
involves presenting the blade to the head of the family unit, who will in turn
present it to a master smith. The blade will undergo a series of trials, to
demonstrate its strength, balance, and sharpness. Generally, the master smith
overseeing the ceremony is a close associate of the family, and a smith
rejecting a blade is almost unheard of.

People reared in **Cinderhome** carry this blade with them for the rest of their life.
It is usually a simple, unadorned blade, with a simple wooden or horn grip.
The quality of the blade is a source of pride and some standing within
**Cinderhome** society. Of course, members of the upper houses will commision
experienced craftsmen to "mentor" young nobles. In such cases, a blade that is
*too* good becomes a mark of shame, as it gives away the charade.


