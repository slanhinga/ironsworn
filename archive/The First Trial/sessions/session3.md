The First Trial - Session 3
===========================

#Contents

------------------------------------------------------------------------------

>I seem to have had a bunch of bad rolls, which keep piling on to one another.
Let's try and take off in a different direction. How can I take a **miss** and
turn it into an interesting plotline?

------------------------------------------------------------------------------
# Recap

Shortly after leaving [Cinderhome](../Cinderhome.md), [Draven](../jasonpc.md)
was caught by a late season winter storm. After finally recovering himself, he
was led deeper into the wild woods by the spectral figure of his mother (even
though he never knew her). He eventually came to a fetid clearing and was
attacked by a strange plant/fungus creature. He found himself trapped in the
ruins of an abandoned hut, where he lost consciousness. Awaking the next
morning, he found the hut had been overgrown with plant material, making an easy
exit impossible.

------------------------------------------------------------------------------
# Exploring the cabin

    **Gather Information**: Action 4 (1+2+1), Challenge 1, 4 (**weak hit**)
    
Add and extra momentum, and the information complicates my quest. 

    `Oracle 2: Theme` 2 Ability
    `Oracle 18: Major Plot Twist` 91 Unexpected powers or abilities are revealed
    
Draven searches the hut, and finds a secret compartment, with a small pile of
journals. 
