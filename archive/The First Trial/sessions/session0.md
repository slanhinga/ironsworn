Session 0 (Sun, 15 Jan 2023)
============================

# Contents

- [Go thru the world building work book.](#Go thru the world building work book.)
- [Create my character](#Create my character)
    - [Assets](#Create my character#Assets)
    - [Stats](#Create my character#Stats)
- [Developing the World](#Developing the World)
    - [Cinderhome](#Developing the World#Cinderhome)
    - [Listeners](#Developing the World#Listeners)

# Go thru the world building work book.
    I selected the truths randomly, and recorded them in [The World](TheWorld.md)
    
------------------------------------------------------------------------------
# Create my character
    
I decided not to embelish much more on the truths that I selected - there's just
enough story there to start putting together my character. I can build as I go.

Magic in this world is rare, but exists. So I'm thinking of a character aspiring
to be a "Listener." And they exist outside the system of circles. They are
generally welcomed in all circles, by custom, and in return remain neutral in
most disputes. They may be asked (and paid?) to act as judges. Some settle in
one place, and take on the role of a local healer/priest/advisor/arbiter. Others
travel, either to seek their own enlightment, or to act as roving diplomats.

ok, so this character might be young adult, setting out on their path to become
a *Listener*. Magic in this world feels like it should be **Heart**. But I
don't think I want to make that my max score - this is something to improve.
I'll make it a 2.

## Assets 

* The character has a history in Cinderhome, so they may have a connection to
  smithing or blade work. He has the *Sunderer* asset, to represent his skill
  with an axe.

* The character has a connection to magic, so I'll take *Sighted* as an asset

* The character was raised by their grandmother, and learned *Herbalism*. 

## Stats

Ok, getting a better picture of this character. They have some fighting ability,
but also a focus on observation and cunning. There is a spiritual connection
that should be represented.

Let's make *Heart* 2 - a good trait, but room to improve
Let's make *Shadow* 3 - this is cunning and connection to the weird
Make *Wits* 2 - this will be needed for rituals.
*Edge* and *Iron* are 1 - some combat ability, but this is not the focus

------------------------------------------------------------------------------
# Developing the World

[The World](TheWorld.md)

## Cinderhome

**[Cinderhome](Cinderhome.md)** is located in the *[Havens](Havens.md)*, near the border with the
*[Hinterlands](Hinterlands.md)*. Rivers provide access for iron ore, mined in the hinterlands, and
for trade with the rest of the **Ironlands**. As [circles](circles.md) go, it is a larger
community, including both a well developed docks district, a bustling central
city, and surrounding farmland. There is a small upper class, consisting of a
half-dozen major families who first settled the area. Despite the harsh
conditions, they enjoy a certain degree of luxury, taking advantage of the hot
springs and geothermal heating in the region.

The *circle* of **Cinderhome** abuts a dark forest. Logging not only provides
valuable resources for the city, it is also essential to keeping the wilds at
bay, so they do not encroach upon the small oasis of civilization.

>    **Oracle 8: Quick Settlement Name Generator**
    
>    * 69 - Lost-
>    * 78 - rock

> ***Quest idea:*** - For three weeks, there has been no word from the camp
**Lost Rock**, a small but permanent community, built around an iron mine.
Yesterday, a body floated into the docks district from the direction of 
**Lost Rock**, with strange wounds on the body. 

Smithing holds such a central place in **Cinderhome** society, and basically
everyone has a basic understanding of the craft. It is a mark of transition to
adulthood when a child crafts a knife on their own. This practice culminates in
a "sheathing ceremony." This comming of age rite is common to all genders, and
involves presenting the blade to the head of the family unit, who will in turn
present it to a master smith. The blade will undergo a series of trials, to
demonstrate its strength, balance, and sharpness. Generally, the master smith
overseeing the ceremony is a close associate of the family, and a smith
rejecting a blade is almost unheard of.

People reared in **Cinderhome** carry this blade with them for the rest of their life.
It is usually a simple, unadorned blade, with a simple wooden or horn grip.
The quality of the blade is a source of pride and some standing within
**Cinderhome** society. Of course, members of the upper houses will commision
experienced craftsmen to "mentor" young nobles. In such cases, a blade that is
*too* good becomes a mark of shame, as it gives away the charade.

## Listeners

The *Listeners* exist outside the politics of circles. By custom they can come or
go among circles freely. The path of a *Listener* can be varied. Some travel
collecting stories, trading song for their supper. Others settle in one place,
acting as mediators and arbiters, due to their customary impartiality.

Some village healers might know some simple charms or poultices, but the
*Listeners* have a true connection to the weird and spiritual forces that occupy
the **Ironlands**. 
