Session 2
=========

# Contents

------------------------------------------------------------------------------
# A complication on the journey

> Draven makes his way to the first waypoint - a permanent shelter for travel
between [Cinderhome](../Cinderhome.md) and [Lost Rock](../LostRock.md) -
following the [violent late winter storm](Session1.md).

Ok, this is the result of `Paying the Price`. 

    **Oracle 18: Major Plot Twist**
        02 It was all a diversion
        07 A dark secret is revealed
        
Hrm, not quite getting anything yet.
    
    **Oracle 17: Mystic Backlash**
        92 You are tormented by an apparition from your past
        
Oh shit. That's good. I think that Draven sees an apparition of his mother, who
had died in childbirth. This is a troublesome, but not dangerous event, but it
does cause him to endure stress.

    roll +heart: Action 10 (6 + 4), Challenge 0, 0 (**miss**)
    
What does this apparition mean? Draven pursues the image thru the forest,
quickly getting himself lost. Even though he never knew his mother, he somehow
knows this spectre is her. 

But, this is also a match - there is some kind of complication or danger. Let's
engage with the combat mechanics - the image leads Draven on a twisted path
until the sun begins to fade, until he comes to the ruins of an abandoned hut. 

There is a short, shambling collection of sticks, hanging moss, and fungus in a
grotesque approximation of human form, with a rough semblance of a face. The
stench of rot is overwhelming.

## plant man

dangerous foe: 2 progress per harm, does 2 harm

|    |    |    |    |    |    |    |    |    |     |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|
| +1 | +2 | +3 | +4 | +5 | +6 | +7 | +8 | +9 | +10 |

    **enter the fray**
    roll +wits(2): Action 8; Challenge 3, 6 (**strong hit**)
    
I have initiative - despite his confusion, Draven comes to his senses just in
time, and instinctually strikes out at the creature with his axe.

    * Strike - miss, I take harm and lose initiative
      The stench of the place causes Draven to swing wide, and the creature
      responds with a swift hit with an arm, that acts more like a cudgel.
    * Clash - ugh, another miss. Draven attempts to get his head right, but a
      flurry of hits, and strange terrain keeps him spinning in circles, -2
      momentum.
    * Face Danger (to try and escape) - miss, again

well shit - this is escalating. 

Draven attempts to evade the plant/fungus creature, but ends up barricading
himself in the abandoned hut. He closes the door and barricades it, before
collapsing and passing out. He has a fitful night of confusing dreams, before
coming to in the morning. He awakens to find the hut covered new plant growth,
securely holding the door fast, and covering over the small windows, letting in
minimal light. Rather than feeling rested, he feels even more deflated, as
though something has sapped his energy.

Endure stress (dangerous foe): Action 7; Challenge 6, 6 (**strong hit, match**)

I choose to "embrace the darkness" and take +1 momentum (which I seem to have a
hard time generating).

*The twist* - as I collect myself, pacing around the abandoned hut, considering
my situation, I find a trapdoor to a basement under the hut. In said basement,
I manage to find a dusty lantern, which I light - who knows how long it will
illuminate - and find a collection of books, and jars, covered in dust. Most of
the books collapse under minimal handling, I don't know what aid I will find in
them. 

This seems like a good time to `Gather Information`:

    **Gather Information**: Action(*Wits* +2) 7; Challenge 2, 7 (**weak hit**) 
    
> On a **weak hit**, the information complicates your quest or introduces a ndw
danger. Envision what you discover (*Ask the Oracle* if unsure), and take +1
momentum.

Definitely consulting an oracle here:

... oh wait

I have the [Sighted](../jasonpc.md#Sighted (path))
