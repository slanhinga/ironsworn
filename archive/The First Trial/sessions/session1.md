Session 1 - The Inciting Incident
=================================

# Contents

------------------------------------------------------------------------------
# Inciting Incident

A couple oracles:

* `Oracle 1: Action` - 76 Betray
* `Oracle 2: Theme` - 9 Wound
* `Oracle 10: Character Role` - 67 Herder
* `Oracle 9: Settlement Trouble` - 34 Unjust leadership

"Betray," "wound," and "unjust leadership" seem to go together.

Look at [The World](TheWorld.md) to see if anything clicks.

Ok, I already have a decent quest idea in my 
[Session 0](Session 0 (Sun, 15 Jan 2023\).md) 
(I'm gonna tweak it maybe - just the time scale):

> ***Quest idea:*** - Since last season, there has been no word from the camp
**Lost Rock**, a small but permanent community, built around an iron mine.
Yesterday, a body floated into the docks district from the direction of 
**Lost Rock**, with strange wounds on the body. 

This is similar to the ***Iron*** truth in Session 0.

I *Swear an Iron Vow*: "I will determine what trouble, has afflicted **Lost
Rock**." This is a *dangerous* vow/quest. 

> `roll +heart +1(bond)`: Action 5, Challenge 5, 8 (**miss**)

Well shit. I "face a significant obstacle" before I can begin my quest (it's the
inciding incident, so I can't really *Forsake My Vow*). Also, Suffer -2 momentum

# Starting a journey

Hey, it's late. I'm back home. Let's consult an oracle:

> * `Oracle 18: Major Plot Twist` - 43 You and an enemy share a common goal
  * `Oracle 2: Theme` - 57 Blood

----

There was a ton of wind in ABQ today, so let's say that I encounter a violent
storm, shortly after leaving [Cinderhome](Cinderhome.md), and am forced to
struggle to find shelter. I'll `Undertake A Journey`, and treat the first
segment as an automatic **miss**. This is probably a Troublesome journey - there
is regular commerce between two communities, so there must be decently well
traveled routes. 

> I'm focusing here on engaging with the mechanics of the system. Travel seems a
pretty important part of this game, as well as maintaining multiple progress
tracks. Using the `Undertake A Journey` has me start another track, and I get to
also use the `Pay the Price` move. 

A logical option for the `Pay the Price` result is to make a `Face Danger` move.

The weather remains hostile to river travel, and since the body floated down
river, boats have been reluctant to go up river. So, Draven sets off on foot,
along a well known path. After two days' travel, a violent winter storm whips
up, strange this late in the season. 

> `roll +wits `: Action 8 (6 + 2), Challenge 1, 9 (**weak hit**)

I choose to endure *1 harm*. 

> `roll +health `: Action 10 (6 + 4), Challenge 4, 6 (**strong hit**)

I "embrace the pain" and take +1 momentum. Draven struggles in the storm to find
the smallest alcove in which he can take shelter, and create a meager fire of
small sticks. He survives the night, but the cold does its work on him.

------------------------------------------------------------------------------

As soon as early dawn lightens the gray sky, Draven collects himself and
continues his journey with greater determination.

> `roll +wits`: Action 8 (6 + 2), Challenge 2, 9 (**weak hit**)

Draven makes progress the next day, as well as he can, and reaches the permanent
shelter he had aimed for the day before. Unfortunately, the frost and snow
covered landscape did not offer anything, so I suffer -1 supply.

> `roll +wits`: Action 3 (1 + 2), Challenge 3, 9 (**miss**)

78 A surprising development complicates your quest.

I'll leave this as a cliff hanger for tonight.
