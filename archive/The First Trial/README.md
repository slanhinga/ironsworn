The First Trial
===========================

> *An **Ironsworn** solo adventure*

by Jason Slanga <jason@anhinga.net>

* [blank character sheet](./ironsworn charactersheet.md)
* [jasonpc](jasonpc.md)

# markdown conventions

marking progress tracks

* 0 :
* 1 : .
* 2 : o
* 3 : O
* 4 : X
