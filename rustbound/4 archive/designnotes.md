Design Notes
============

# Contents

- [Design Thoughts](#Design Thoughts)
- [Game Structure](#Game Structure)
    - [Missions](#Game Structure#Missions)
- [Gumption](#Gumption)
- [Moves](#Moves)
- [Bonds](#Bonds)
- [Assets](#Assets)
    - [Paths](#Assets#Paths)
    - [Companions](#Assets#Companions)
    - [Combat](#Assets#Combat)
    - [Rituals](#Assets#Rituals)
- [The World](#The World)
    - [The Fall](#The World#The Fall)

# Design Thoughts

* There is some kind of **threat**:
    * zombies/mold people
    * aliens
    * radiation
    * plague
    * raiders
    * 
* Progress often goes in **missions**:
    * escort someone to safety
    * get to a place of hope
    * build something
    * swear vengeance
* but there should also be a free-roaming move
    * like when you're forced to flee unexpectedly
* Rather than **momentum**, these fictions usually involve **hope**
    * in this context **hope** is the opposite of **stress**
    * it becomes a resource in times of hardship
    * and when it is lacking, even easy tasks seem difficult.
* move idea - **Flee a sanctuary**:
    * when you're forced to abandon a stronghold or camp:
        * because of internal pressures - politics, cult, persecution
        * because of external pressures - raiders, zombie invasion,
          contamination
        * could affect **supply**, **hope**
    * possible world descriptors:
        * there is almost no society. some people may gather together in small
          cooperatives, or bands of raiders. But ultimately, no one faction has
          a majority of the power

------------------------------------------------------------------------------
# Game Structure

## Missions

Rather than vows, the goals are expressed as missions, either a mission that you
set for yourself (e.g. fulfill a promise to a lost loved one), or one that is 
assigned to you (e.g. escort the special child to the resistance base).

------------------------------------------------------------------------------
# Gumption

You know what. Fuck it. **momentum** is now **gumption**. I fucking love that
word. It expresses exactly what I mean. Plus, Nick Offerman wrote a book
entitled *Gumption*, and dammit that's good enough for me.

------------------------------------------------------------------------------
# Moves

------------------------------------------------------------------------------
# Bonds

Bonds could be past as well as current. For example, you might have a bond to a
loved one who has passed, an institution that existed before 
[The Fall](#The World#The Fall)

------------------------------------------------------------------------------
# Assets

## Paths

* Gunner
* Computer Tech
* Prepper
* Homesteader
* Military
* Science
* Medical
* 

## Companions

## Combat

* Ranged Accuracy
* MMA
* Brawl
* Knife-fighting
* 

## Rituals

------------------------------------------------------------------------------
# The World

## The Fall
