The World Oracle
================

# Contents

- [unsorted list of questions and prompts](#unsorted list of questions and prompts)
- [Your Truths](#Your Truths)
    - [Communities](#Your Truths#Communities)

# unsorted list of questions and prompts

types of threats
- hordes, like zombies or fungus people
- something unseen but lethal
- gotdam robuts!

weird stuff
- can people do magic now?
- some sort of weird dimensional rift?
- isn't the real world fucked up and dangerous enough?

society
- there is a governmental organization which maintains strict enforcement
  districts
- there is no government. people tend to group together for safety, and some
  establish long term settlements. there are rumors of a safe zone

nature
- they always told us the seas would rise. now, there's nothing that's not at
  least half submerged in water
- fuck it's cold
- gray


These can be grouped:

* **The Fall**/**The Threat** - what is it that has brought down the world?:
    1. alien
    2. magick
    3. extra dimensional
    4. spiritey wyrdness
    5. biological - zombies, mushroom people, plagues, bullshit, nature
    6. tech - robots, ai, cyberpunk
* **Society**:
    * runs a spectrum between *crowded/militarized* and *disperse/anarchy*
    * how do communities organize themselves?:
        * warbands/small contained groups for safety
        * ultra-libertarian anarcho-capitalism
        * fortified communities in secure locations, and smaller camps and
          wandering bands
        * authoritarian control over strictly monitored and regulated districts
* **The Weird** - what *extra-mundane* element(s) affect(s) the world?:
    * magick
    * chaos forces
    * spirits and witchery
    * technology
    * bio-threat
    * alien influence
    * nothing - isn't the self-destructive nature of humanity enough for
      you?

# Your Truths

> This section is probably where a lot of the reskinning effort will go.

## Communities

* [ ] life on the move, or cutthroat trading posts
* [ ] 
