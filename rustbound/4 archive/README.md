Ironsworn - RUSTBOUND
=====================

This is my personal reskin/hack of ***Ironsworn***. It is heavily inspired by
***Apocalypse World***, but veers a bit more towards *The Road*, rather than
*Mad Max*. 

# Contents

- [Rules Sections](#Rules Sections)
- [Appendix N](#Appendix N)

# Rules Sections

1. [Character](character.md)
2. 

------------------------------------------------------------------------------
# Appendix N

* ***Apocalypse World***
* *The Road*
* *The Last of Us*
* *Birdsong*
* *A Quiet Place*
* *The Walking Dead*
* 
