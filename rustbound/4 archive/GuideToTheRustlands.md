Guide to the Rustlands
======================

# Contents

- [Design Objectives](#Design Objectives)
- [Using Playing Cards](#Using Playing Cards)
    - [Why?](#Using Playing Cards#Why?)
    - [Action Rolls](#Using Playing Cards#Action Rolls)
        - [Reshuffling the Action Deck](#Using Playing Cards#Action Rolls#Reshuffling the Action Deck)
    - [Oracles](#Using Playing Cards#Oracles)
        - [Generating d100](#Using Playing Cards#Oracles#Generating d100)
- [The World](#The World)
    - [Truths](#The World#Truths)

Lorem ipsum, dolor sit amet.

------------------------------------------------------------------------------
# Design Objectives

A lot of the themes that I want to explore are already in *Ironsworn*, so I plan
to start by changing as little as possible and slowing making changes. 

Travel seems an important of some of the inspirational media, and is pretty well
implemented in *Ironsworn*, so I plan to lean heavinly on that.

------------------------------------------------------------------------------
# Using Playing Cards

## Why?

1. Why not? Playing cards are cool.
2. I'm drawn to the idea of travel, as a central theme in the game. I feel like
   using playing cards would make it easier to play in places where dice might
   be inconvenient.
3. Suits can add thematic elements to writing new **oracles**. 
4. As time goes on, the probabilities will shift, and I think that's
   interesting. It also makes reshuffling the deck meaningful. 


## Action Rolls

> ***tl;dr** - Instead of rolling challenge dice, draw two cards between 2-9.
Instead of rolling an action die, draw a card from 10-J-Q-K-A to get a modifier
from +0 to +4. You want to get **greater than or equal** to the challenge cards
(as opposed to **greater than** as in *Ironsworn*)*

Divide a full deck into two piles: 

* challenge pile: 2-9
* action pile: 
    * 10 = +0
    * J = +1
    * Q = +2
    * K = +3
    * A = +4

In order to make an **action roll**, draw two **challenge cards**, and one **action card**.

    **action score** = **stat** + **action card** + **adds**

If your **action score** is *greater than or equal to* a challenge card, it
counts as 1 *success*. 

* **strong hit**: 2 successes
* **weak hit**: 1 success
* **miss**: 0 successes

### Reshuffling the Action Deck

I'm not sure how much shifting probabilities will impact actual game play. But,
I'm assuming that a hot streak will eventually give way to a cold streak. As
such, a player might want to reshuffle the deck to avoid an inevitable run of
bad luck. Add the following to the *Sojourn* move:

**Reset Fate**

* Reshuffle both **action decks**.

> I considered adding another cost to this, like reseting momentum, but none of
the other options for *Sojourn* had additional costs. Also, reshuffling the deck
would already take away one of the player's other recovery options, and the move
itself is limited narratively (i.e. you have to be in a community). 

------------------------------------------------------------------------------
## Oracles

### Generating d100

Draw 2 cards (ignoring JQK) - A=1, 10=0 (00 = 100). There is a slightly lower
probability to get doubles (1/13 instead of 1/10) for a newly shuffled deck.

------------------------------------------------------------------------------
# The World

Initially just use the truth categories as given, but create new options. This
is how to start to create setting.

## Truths


* **The Old World** - how did the world collapse?

* **~~Iron~~ Rust** - what is technology like in this world?

* **Legacies** - what is left of the Old World?

* **Communities** - is there some authoritarian government regime? how do people
  trade? are there quarantine zones?

* **Leaders**

* **Defense**

* **Mysticism** - do people have new abilities or senses? new entities to
  commune with?

* **Religion** - how have the seeming 'end times' affected religion?

* **Firstborn** - hrm, I'm not sure how to replace this. 

* **~~Beasts~~ Nature** - how has nature changed/become a threat? climate
  change? animal mutations? how have horrors affected nature?

* **Horrors** - what physical threat looms over the world? technology?
  biological threats (virus, fungus, zombies, etc.)? otherworldly entities?
