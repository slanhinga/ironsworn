The World
=========

# Contents

- [Rustlands Truths](#Rustlands Truths)
    - [The Old World](#Rustlands Truths#The Old World)
    - [Iron](#Rustlands Truths#Iron)
    - [Legacies](#Rustlands Truths#Legacies)
    - [Communities](#Rustlands Truths#Communities)
    - [Leaders](#Rustlands Truths#Leaders)
    - [Defense](#Rustlands Truths#Defense)
    - [Mysticism](#Rustlands Truths#Mysticism)
    - [Religion](#Rustlands Truths#Religion)
    - [Firstborn](#Rustlands Truths#Firstborn)
    - [Beasts](#Rustlands Truths#Beasts)

# Rustlands Truths

------------------------------------------------------------------------------
## The Old World

------------------------------------------------------------------------------
## Iron

------------------------------------------------------------------------------
## Legacies

------------------------------------------------------------------------------
## Communities

* [ ] Out here, it's everyone for themselves. A few groups might be able to come
      together, for safety on the road, but truly long term camps are rare. 

------------------------------------------------------------------------------
## Leaders

------------------------------------------------------------------------------
## Defense

* [ ] Here in the Rustlands, supplies are too precious, and the lands are too
      sparsely populated, to support organized fighting forces. When a community
      is threatened, the people stand together to protect their own.

* [ ] The Coalition maintains control over strongly fortified settlements. The
      people accept, perhaps begrudgingly, this oversight, because it provides
      safety and stability. Who knows what happens in the Rustlands between
      Coalition strongholds?
      
* [ ] Larger communities will often take over some existing fortified
      location, like a prison or military base, or some other easily secured
      location, like a shopping mall. These locations may have stockpiled
      resources, but in the long run any sizeable group will need to establish
      some form of agriculture, or conduct regular supply runs. 

------------------------------------------------------------------------------
## Mysticism

------------------------------------------------------------------------------
## Religion

------------------------------------------------------------------------------
## Firstborn

------------------------------------------------------------------------------
## Beasts
