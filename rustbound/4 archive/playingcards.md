Playing Card Mechanics
======================

Game play make use of two decks of standard playing cards.

* Challenge deck
* Oracle deck

# Contents

- [Undertaking Challenges](#Undertaking Challenges)
    - [Special Combinations](#Undertaking Challenges#Special Combinations)
        - [Pairs](#Undertaking Challenges#Special Combinations#Pairs)
        - [Suited](#Undertaking Challenges#Special Combinations#Suited)
- [World Building](#World Building)
    - [How to use this oracle](#World Building#How to use this oracle)
    - [The threat](#World Building#The threat)
    - [Communities](#World Building#Communities)
    - [](#World Building#)
- [Oracles](#Oracles)
- [Moves](#Moves)

# Undertaking Challenges

At the beginning of play, split your **challenge deck** into two piles:

    * **action deck**: J-Q-K-A
        * action cards give the following modifiers:
        * J = +1
        * Q = +2
        * K = +3
        * A = +4
    * **target deck**: 2-9

To **undertake a challenge**, draw two cards from the the **target deck** and
one card from the **action deck**. Add the modifier from your **action card** to
one of your stats, along with any other *adds*, and compare the result to your
two target numbers. For each target number that you *meet or exceed*, earn one
success.

    * 2 successes: strong hit
    * 1 success: weak hit
    * 0 successes: miss

## Special Combinations

### Pairs

### Suited

------------------------------------------------------------------------------
# World Building

> What are some core elements? There is some kind of strange threat that has
transformed the world. Society as we know it has been upheaved. Resources have
become scarce. The environment is dangerous. 

## How to use this oracle

This is a special oracle. 

## The threat

## Communities

## 

------------------------------------------------------------------------------
# Oracles

------------------------------------------------------------------------------
# Moves
